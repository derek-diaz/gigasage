<?php 
	$pageTitle = 'Contacto - Diaz Correa Dental';
	$link1 = '';
	$link2 = '';
	$link3 = '';
	$link4 = '';
	$link5 = '';
	$link6 = 'active';
	$language_alt = 'contact.php';
	include ('includes/header.php');
	$main = 0;
	$lang = 'esp';
	include ('includes/send_mail.php');
?>

<div id="content">

<h1>CONTACTO</h1>
<br/>

<iframe class="google_map" width="400" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;t=h&amp;msa=0&amp;msid=112574469226297152600.00048e0daba543135745c&amp;ll=18.112689,-66.167157&amp;spn=0.001785,0.00228&amp;z=18&amp;output=embed"></iframe>
<h3>Oficina Dental Diaz y Correa</h3>
<h3>Barbosa #58 Cayey, PR 00736</h3>
<h3>Tel: 787-263-1313</h3>
<h3>Fax: 787-263-4597</h3>
<br/>
<h3>Horario</h3>	
	<ul>
		<li>Lunes     - 8:00am - 3:00pm</li>
		<li>Martes    - 8:30am - 6:00pm</li>
		<li>Miercoles - 8:30am - 6:00pm</li>
		<li>Jueves    - 8:30am - 6:00pm</li>
		<li>Viernes   - 8:00am - 3:00pm</li>
		<li>Sabado    - Cerrado</li>
		<li>Domingo   - Cerrado</li>
	</ul>
<br/>
<h3>Contacto</h3>
<?php

if ($main == 0){
echo'
<p>Nosotros estamos constantemente buscando maneras de mejorar nuestro servicio. Si tiene alguna sugerencia o algun comentario en algo que deberiamos mejorar, nos puedes dejar saber llenando este formulario. La informacion es completamente confidencial.</p>

<fieldset>
<form action="contacto.php" method="post">
<p class="form"><strong>Email:</strong> <input type="text" name="email"/></p>
<p class="form"><strong>Titulo:</strong> <input type="text" name="title"/></p>
<p class="form"><strong>Mensaje:</strong><br/>
<textarea name="msg" rows="6" cols="80"></textarea></p>
<p><input type="submit" name="submit" value="Enviar"/></p>
<input type="hidden" name="submitted" value="TRUE" />
</form>
</fieldset>';
}

if ($main == 1){
 if($lang == 'esp'){
 echo'<p>Su Mensaje ha sido enviado!</p>';
 }else{
 echo'<p>Your message has been sent!</p>';
 }
}

if($main == 2){
	echo'<h1 class="error">SERVER ERROR!</h1>';
}

if($main == 3){
	echo'<h1 class="error">ERROR!</h1>';
	
	if($lang == 'esp'){
 		echo'<p>Ocurrieron los siguientes errores: <br/>';
 	 }else{
		 echo'<p>The following error(s) occurred: <br/>';
	 }

 	
      foreach ($errors as $msg){//print each error
        echo " - $msg <br/>\n";
      }
      
      if($lang == 'esp'){
 		echo ' <br/><br/>Por Favor, intentelo otravez!</p> ';
	 }else{
		 echo ' <br/><br/>Please Try Again!</p> ';
	 }

        
echo'

<fieldset>
<form action="contacto.php" method="post">
<p class="form"><strong>Email:</strong> <input type="text" name="email"/></p>
<p class="form"><strong>Titulo:</strong> <input type="text" name="title"/></p>
<p class="form"><strong>Mensaje:</strong><br/>
<textarea name="msg" rows="6" cols="80"></textarea></p>
<p><input type="submit" name="submit" value="Enviar"/></p>
<input type="hidden" name="submitted" value="TRUE" />
</form>
</fieldset>';



}



?>

</div>

<?php 
include ('includes/footer.php');
?>


