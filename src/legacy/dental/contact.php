<?php 
	$pageTitle = 'Contact - Diaz Correa Dental';
	$link1 = '';
	$link2 = '';
	$link3 = '';
	$link4 = '';
	$link5 = '';
	$link6 = 'active';
	$language_alt = 'contacto.php';
	include ('includes/header-eng.php');
	$main = 0;
	$lang = 'eng';
	include ('includes/send_mail.php');
?>

<div id="content">

<h1>CONTACT US</h1>
<br/>

<iframe class="google_map" width="400" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;t=h&amp;msa=0&amp;msid=112574469226297152600.00048e0daba543135745c&amp;ll=18.112689,-66.167157&amp;spn=0.001785,0.00228&amp;z=18&amp;output=embed"></iframe>
<h3>Oficina Dental Diaz y Correa</h3>
<h3>Barbosa #58 Cayey, PR 00736</h3>
<h3>Phone: 787-263-1313</h3>
<h3>Fax: 787-263-4597</h3>
<br/>
<h3>Operating Hours</h3>	
	<ul>
		<li>Monday     - 8:00am - 3:00pm</li>
		<li>Tuesday    - 8:30am - 6:00pm</li>
		<li>Wednesday  - 8:30am - 6:00pm</li>
		<li>Thursday   - 8:30am - 6:00pm</li>
		<li>Friday     - 8:00am - 3:00pm</li>
		<li>Saturday   - Closed</li>
		<li>Sunday     - Closed</li>
	</ul>
<br/>
<h3>Contact Form</h3>
<?php

if ($main == 0){
echo'
<p>We are constantly looking for ways to improve our service to you. If you have any suggestions o simply want to tell us something we need to improve, you can let us know by filling this form. The information is confidential.</p>

<fieldset>
<form action="contact.php" method="post">
<p class="form"><strong>Email:</strong> <input type="text" name="email"/></p>
<p class="form"><strong>Title:</strong> <input type="text" name="title"/></p>
<p class="form"><strong>Message:</strong><br/>
<textarea name="msg" rows="6" cols="80"></textarea></p>
<p><input type="submit" name="submit" value="Submit"/></p>
<input type="hidden" name="submitted" value="TRUE" />
</form>
</fieldset>';
}

if ($main == 1){
 if($lang == 'esp'){
 echo'<p>Su Mensaje ha sido enviado!</p>';
 }else{
 echo'<p>Your message has been sent!</p>';
 }
}

if($main == 2){
	echo'<h1 class="error">SERVER ERROR!</h1>';
}

if($main == 3){
	echo'<h1 class="error">ERROR!</h1>';
	
	if($lang == 'esp'){
 		echo'<p>Ocurrieron los siguientes errores: <br/>';
 	 }else{
		 echo'<p>The following error(s) occurred: <br/>';
	 }

 	
      foreach ($errors as $msg){//print each error
        echo " - $msg <br/>\n";
      }
      
      if($lang == 'esp'){
 		echo ' <br/><br/>Por Favor, intentelo otravez!</p> ';
	 }else{
		 echo ' <br/><br/>Please Try Again!</p> ';
	 }

        
echo'

<fieldset>
<form action="contact.php" method="post">
<p class="form"><strong>Email:</strong> <input type="text" name="email"/></p>
<p class="form"><strong>Title:</strong> <input type="text" name="title"/></p>
<p class="form"><strong>Message:</strong><br/>
<textarea name="msg" rows="6" cols="80"></textarea></p>
<p><input type="submit" name="submit" value="Submit"/></p>
<input type="hidden" name="submitted" value="TRUE" />
</form>
</fieldset>';



}



?>

</div>

<?php 
include ('includes/footer-eng.php');
?>


