<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="es" />
<meta name="description" content="Oficina Dental Diaz y Correa" />
<title><?php echo $pageTitle; ?></title>
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="slider.css" type="text/css" media="screen" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
</head>

<body>
<div id="head">
<div id="header">
<img alt="Diaz Correa Dental" src="images/logo3.png" />
<a href="http://www.facebook.com/pages/Cayey-Puerto-Rico/Diaz-Correa-Dental/138994009469192"><img class="facebook" alt="Buscanos en Facebook" src="images/facebook.png" /></a>
<a href="<?php echo $language_alt; ?>"><img class="facebook" alt="English Version" src="images/eng.png" /></a>


</div>

<div id="menu">
		<div id="menu_item">
			<ul>
				<li><a class="<?php echo $link1 ?>" href="index.php">Home</a></li>
				<li><a class="<?php echo $link2 ?>" href="servicios.php">Servicios</a></li>
				<li><a class="<?php echo $link3 ?>" href="dentistas.php">Dentistas</a></li>
				<li><a class="<?php echo $link4 ?>" href="equipo.php">Equipo</a></li>
				<li><a class="<?php echo $link5 ?>" href="facilidades.php">Facilidades</a></li>
				<li><a class="<?php echo $link6 ?>" href="contacto.php">Contacto</a></li>
				<li class="last"><a class="<?php echo $link7 ?>"  href="cita.php">Citas</a></li>
			</ul>
		</div>	
</div>
</div>
<div id="slider">
<ul class="slideshow">
	<li class="show"><img src="images/slider/s1.jpg" width="800" height="250" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor."/></li>
	<li><img src="images/slider/s2.jpg" width="800" height="250" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor."/></li>
	<li><img src="images/slider/s3.jpg" width="800" height="250" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor."/></li>
</ul>
</div>
