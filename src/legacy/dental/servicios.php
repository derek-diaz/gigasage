<?php 
	$pageTitle = 'Servicios - Diaz Correa Dental';
	$link1 = '';
	$link2 = 'active';
	$link3 = '';
	$link4 = '';
	$link5 = '';
	$link6 = '';
	$language_alt = 'services.php';
	include ('includes/header.php');
?>

<div id="content">

<h1>SERVICIOS</h1>
<p><img class="smile" alt="smile" src="images/smile2.jpg"/>Nuestros servicios de basan en dise&ntilde;ar e implementar un programa personalificado para resolver sus necesidades dentales. </p>
<br/>
<h3>Ofrecimientos</h3>	
	<ul>
		<li>Odontolog&iacute;a Preventiva</li>
		<li>Odontolog&iacute;a Reconstructiva</li>
		<li>Implantes Dentales</li>
		<li>Coronas y Puentes</li>
		<li>Blanqueamiento de Dientes en 1 hora (ZOOM!)</li>
	</ul>


<img src="images/assoc_footer.png" alt="seals" />

</div>

<?php 
include ('includes/footer.php');
?>


