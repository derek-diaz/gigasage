<?php 
	$pageTitle = 'Dentistas - Diaz Correa Dental';
	$link1 = '';
	$link2 = '';
	$link3 = 'active';
	$link4 = '';
	$link5 = '';
	$link6 = '';
	$language_alt = 'dentists.php';
	include ('includes/header.php');
?>

<div id="content">

<h1>DENTISTAS</h1>

<h2>Dr. Jaime E Diaz Vicente DMD</h2>
<p><img class="smile" alt="smile" src="images/staff/jaime.jpg"/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Vivamus tincidunt lacus libero, eu placerat ipsum. Nulla facilisi. Cras vestibulum lobortis tortor, non convallis massa commodo vitae. 
Nullam eu lacus elit, sit amet tincidunt lorem. Ut odio mi, tincidunt ac fringilla ut, ornare ut sapien. 
Nullam sit amet tellus ut quam mollis interdum non interdum arcu. Nunc leo ipsum, posuere ut rhoncus ac, cursus sit amet ante. Cras rutrum porta enim. 
Vivamus vel neque in mauris iaculis egestas quis nec massa. Fusce ac augue urna, at pretium ante. Donec nec urna sit amet diam mattis viverra ut et tortor.</p>
<br/>
<h2>Dra. Desiree Correa Carro DMD</h2>
<p class="dentists"><img class="smile" alt="smile" src="images/staff/desiree.jpg"/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Vivamus tincidunt lacus libero, eu placerat ipsum. Nulla facilisi. Cras vestibulum lobortis tortor, non convallis massa commodo vitae. 
Nullam eu lacus elit, sit amet tincidunt lorem. Ut odio mi, tincidunt ac fringilla ut, ornare ut sapien. 
Nullam sit amet tellus ut quam mollis interdum non interdum arcu. Nunc leo ipsum, posuere ut rhoncus ac, cursus sit amet ante. Cras rutrum porta enim. 
Vivamus vel neque in mauris iaculis egestas quis nec massa. Fusce ac augue urna, at pretium ante. Donec nec urna sit amet diam mattis viverra ut et tortor.</p>


</div>

<?php 
include ('includes/footer.php');
?>


