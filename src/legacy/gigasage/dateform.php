<?php
	$page_title = 'Welcome to Gigasage.com! - Date Form Test';
	$link1 = "";
	$link2 = "";
	$link3 = "active";
	$link4 = "";
	$link5 = "";
	$link6 = "";
	include ('includes/header.html');
	include ('includes/sidebar.html');

?>

 	<div id="content_inside_main">		
<?php
    function make_calendar_pulldown(){
        $months = array(1 => 'January', 'Febuary', 'March', 'April', 'May', 'June',
                        'July', 'August', 'September', 'October', 'November', 'December');

        echo '<select name="month">';

        foreach ($months as $key => $value){
            echo "<option value=\"$key\">$value</option>\n";
        }

        echo '</select>';

        echo '<select name="day">';

        for ($day = 1; $day <= 31; $day++){
            echo "<option value=\"$day\"> $day</option>\n";
        }
        echo '</select>';

        echo '<select name="year">';

        for ($year = 2010; $year <= 2020; $year++){
            echo "<option value=\"$year\"> $year</option>\n";
        }
        echo '</select>';


       }



       echo '<h1>Select a Date:</h1>
       <form action="dateform.php" method="post">';

       make_calendar_pulldown();

       echo '</form>';

?>

	</div>	
<?php
	include ('includes/footer.html');
?>		
