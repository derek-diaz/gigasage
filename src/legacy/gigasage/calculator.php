<?php
	$page_title = 'Welcome to Gigasage.com! - Calculator Test';
	$link1 = "";
	$link2 = "active";
	$link3 = "";
	$link4 = "";
	$link5 = "";
	$link6 = "";
	include ('includes/header.html');
	include ('includes/sidebar.html');

?>

 			<div id="content_inside_main">				
				<?php 
				//If submit button has been pressed Execute this:
				if (isset($_POST['submitted'])){
				 //Form Validation
				 if(is_numeric($_POST['quantity']) && is_numeric($_POST['price']) && is_numeric($_POST['tax'])){
				 	$total =  ($_POST['quantity'] * $_POST['price']);
				 	$taxRate = ($_POST['tax'] / 100);
				 	$total += ($total * $taxRate);
				 	
				
					//Printing results:
					
					echo '<h1> Total Costs</h1>
					
					<p> Total Cost of purchasing ' . $_POST['quantity'] . ' widget(s) at $' . number_format($_POST['price'], 2) . ' each , including
					 a tax rate of ' .  $_POST['tax'] . '%, is $' . number_format ($total, 2) . '.</p>'; 

				 }else{ // Overall validation FAILED!
				 	echo '<h1>Error!</h1>
				 	
				 	<p class="error">Please enter a valid quantity, price and tax. </p>';
				 }// End If Validation
				 
			  }//End If isset
			  ?>
			  
			  <h1>Widget Cost Calculator </h1>
			  
			  <form action="calculator.php" method="post">
			  
			  <p>Quantity: <input type="text" name="quantity" size="5" maxlength="5" value="<?php if (isset($_POST['quantity'])) echo $_POST['quantity']; ?>" /></p>
			  
			  <p>Price: <input type="text" name="price" size="5" maxlength="10" value="<?php if (isset($_POST['price'])) echo $_POST['price']; ?>" /></p>
			  
			  <p>tax (%): <input type="text" name="tax" size="5" maxlength="5" value="<?php if (isset($_POST['tax'])) echo $_POST['tax']; ?>" /></p>
			  
			  <p><input type="submit" name="submit" value="Calculate!" /></p>
			  
			  <input type="hidden" visible="false" name="submitted" value="1" />		  		  
			  
			  </form>
			  
			  
			</div>	
<?php
	include ('includes/footer.html');
?>		
