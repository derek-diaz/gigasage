new TWTR.Widget({
  				version: 2,
  				type: 'profile',
  				rpp: 4,
  				interval: 6000,
  				width: 240,
  				height: 300,
  				theme: {
    				shell: {
     				 background: '#E2E2E2',
      				 color: '#000000'
    						},
    				tweets: {
     				 background: '#E2E2E2',
     				 color: '#000000',
     				 links: '#4aed05'
    						}
  				},
  			features: {
    				scrollbar: false,
    				loop: false,
    				live: false,
    				hashtags: true,
    				timestamp: true,
   				avatars: false,
    				behavior: 'all'
 					 }
				}).render().setUser('gigabit77').start();
