<?php
	$page_title = 'Welcome to Gigasage.com!';
	$link1 = "active";
	$link2 = "";
	$link3 = "";
	$link4 = "";
	$link5 = "";
	$link6 = "";
	include ('includes/header.html');
?>
<div id="content">
		<div id="article">
				<h1>Important <span>By Derek Diaz | 07-21-10</span></h1>
				<img src="images/newsDivider.png" alt="News Divider" />
				<p>Licensed under the <a href="http://creativecommons.org/licenses/by/2.5/">Creative Commons Attribution 2.5</a>. I have left the actual text design style very basic for you to add your own text / header / quotes / list etc. styles yourself.</p>
		</div>
		<div id="article">
				<h1>header two <span>By Derek Diaz | 07-21-10</span></h1>
				<img src="images/newsDivider.png" alt="News Divider" />

				<p>Volutpat at varius sed sollicitudin et, arcu. Vivamus viverra. Nullam turpis. Vestibulum sed etiam. Lorem ipsum sit amet dolore. Nulla facilisi. Sed tortor. Aenean felis. Quisque eros. Cras lobortis commodo metus. Vestibulum vel purus. In eget odio in sapien adipiscing blandit. Quisque augue tortor, facilisis sit amet, aliquam, suscipit vitae, cursus sed, arcu lorem ipsum dolor sit amet. Fermentum at, varius pretium, elit. Mauris egestas scelerisque nunc. Mauris non ligula quis wisi laoreet malesuada. In commodo. Maecenas lobortis cursus dolor.</p>
		</div>
		<div id="article">
				<h1>header three <span>By Derek Diaz | 07-21-10</span></h1>
				<img src="images/newsDivider.png" alt="News Divider" />

				<p>Volutpat at varius sed sollicitudin et, arcu. Vivamus viverra. Nullam turpis. Vestibulum sed etiam. Lorem ipsum sit amet dolore. Nulla facilisi. Sed tortor. Aenean felis. Quisque eros. Cras lobortis commodo metus. Vestibulum vel purus. In eget odio in sapien adipiscing blandit.</p>	
				<p>Volutpat at varius sed sollicitudin et, arcu. Vivamus viverra. Nullam turpis. Vestibulum sed etiam. Lorem ipsum sit amet dolore. Nulla facilisi. Sed tortor. Aenean felis. Quisque eros. Cras lobortis commodo metus. Vestibulum vel purus. In eget odio in sapien adipiscing blandit.</p>	
				<p>Quisque augue tortor, facilisis sit amet, aliquam, suscipit vitae, cursus sed, arcu lorem ipsum dolor sit amet. Fermentum at, varius pretium, elit. Mauris egestas scelerisque nunc. Mauris non ligula quis wisi laoreet malesuada. In commodo. Maecenas lobortis cursus dolor.</p>
				<p>Quisque augue tortor, facilisis sit amet, aliquam, suscipit vitae, cursus sed, arcu lorem ipsum dolor sit amet. Fermentum at, varius pretium, elit. Mauris egestas scelerisque nunc. Mauris non ligula quis wisi laoreet malesuada. In commodo. Maecenas lobortis cursus dolor.</p>
			</div>
		</div>
		
<?php
	include ('includes/sidebar.html');
	include ('includes/footer.html');
?>		
