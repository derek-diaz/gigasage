<?PHP
///////////////////// TEMPLATE FFS /////////////////////
$template_active = <<<HTML
<table width="550" height="30" border="0" cellpadding="0" cellspacing="0" background="http://ffs.byethost33.com/Content/boxfornews.png">
  <tr> 
    <td height="20"><div align="center"><b><font size="+1" face="Verdana, Arial, Helvetica, sans-serif">{title}</font></b></div></td>
  </tr>
</table>
<table border="1" width="550" cellspacing="0" cellpadding="0" bordercolor="#999999">
  <tr> 
    <td width="100%" style="font:14px Georgia,Verdana,sans-serif; color:#666; text-align:justify"><font face="Verdana, Arial, Helvetica, sans-serif">{short-story}</font></td>
  </tr>
  <tr> 
    <td width="100%" > <table border="0" style="border-top: 1px dotted #f2f3f3; font:11px Georgia,Verdana,sans-serif; color:black" width="408" cellspacing="0">
        <tr> 
          <td width="220"><i>{date} by {author}</i><br></td>
          <td width="168" ><div align=right>[full-link]Read More ...[/full-link] 
              [com-link]comments ({comments-num})[/com-link]<br>
            </div></td>
        </tr>
      </table></td>
  </tr>
</table>
<br />
HTML;


$template_full = <<<HTML
<table width="550" height="30" border="0" cellpadding="0" cellspacing="0" background="http://ffs.freewebsitehost.net/Content/boxfornews.png">
  <tr> 
    <td height="20"><div align="center"><b><font size="+1" face="Verdana, Arial, Helvetica, sans-serif">{title}</font></b></div></td>
  </tr>
</table>
<table border="1" width="550" cellspacing="0" cellpadding="0" bordercolor="#999999">
  <tr> 
    <td width="100%" style="font:14px Georgia,Verdana,sans-serif; color:#666; text-align:justify"><font face="Verdana, Arial, Helvetica, sans-serif">{short-story}</font></td>
  </tr>
  <tr> 
    <td width="100%" > <table border="0" style="border-top: 1px dotted #f2f3f3; font:11px Georgia,Verdana,sans-serif; color:black" width="408" cellspacing="0">
        <tr> 
          <td width="220"><i>{date} by {author}</i><br></td>
          <td width="168" ><div align=right>{comments-num} comments<br>
            </div></td>
        </tr>
      </table></td>
  </tr>
</table>
<br />
HTML;


$template_comment = <<<HTML
<table width="550" height="40" border="2" cellpadding="0" cellspacing="0" bordercolor="#999999">
    <tr>
      
    <td style="border-bottom:1px solid black;"><font face="Verdana, Arial, Helvetica, sans-serif">by 
      <b>{author}</b> @ {date}</font></td>
    </tr>
    <tr>
      
    <td height="40" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif">{comment}</font></td>
    </tr>
  </table>
<br>
HTML;


$template_form = <<<HTML
  <table border="0" width="550" cellspacing="0" cellpadding="0">
    <tr>
      <td width="60">Name:</td>
      <td><input type="text" name="name"></td>
    </tr>
    <tr>
      <td>E-mail:</td>
      <td><input type="text" name="mail"> (optional)</td>
    </tr>
    <tr>
      <td>Smile:</td>
      <td>{smilies}</td>
    </tr>
    <tr>
      <td colspan="2"> 
      <textarea cols="40" rows="6" id=commentsbox name="comments"></textarea><br />
      <input type="submit" name="submit" value="Add My Comment"> 
      <input type=checkbox name=CNremember  id=CNremember value=1><label for=CNremember> Remember Me</label> | 
  <a href="javascript:CNforget();">Forget Me</a>
      </td>
    </tr>
  </table>
HTML;


$template_prev_next = <<<HTML
<p align="center">[prev-link]<< Previous[/prev-link] {pages} [next-link]Next >>[/next-link]</p>
HTML;
$template_comments_prev_next = <<<HTML
<p align="center">[prev-link]<< Older[/prev-link] ({pages}) [next-link]Newest >>[/next-link]</p>
HTML;
?>
